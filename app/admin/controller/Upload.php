<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_EVTP6_PRO前后端分离旗舰版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

namespace app\admin\controller;

/**
 * 上传文件
 * @author 牧羊人
 * @since 2020/11/14
 * Class Upload
 * @package app\admin\controller
 */
class Upload extends Backend
{
    /**
     * 上传图片（支持多图片上传）
     * 备注：1、单文件：file
     *      2、多文件：file[],file[]
     *
     * @author 牧羊人
     * @since 2020-04-21
     */
    public function uploadImage()
    {
        // 错误提示语
        $error = "";
        // 上传图片
        $result = upload_image('file', '', $error);
        if (!$result) {
            return message($error, false);
        }
        // 结果处理
        $list = [];
        foreach ($result as $key => $value) {
            if (is_array($value)) {
                // 二维数组(多图上传)
                foreach ($result as $val) {
                    $list[] = IMG_URL . $val['filepath'];
                }
            } else {
                // 一维数组(单图上传)
                $list = IMG_URL . $result['filepath'];
            }
        }
        return message("上传成功", true, $list);
    }

    /**
     * 上传文件(支持多文件上传)
     * 备注：1、单文件：file
     *      2、多文件：file[],file[]
     *
     * @author 牧羊人
     * @since 2020-04-21
     */
    public function uploadFile()
    {
        $error = "";
        // 上传文件(非图片)
        $result = upload_file('file', '', $error);
        if (!$result) {
            return message($error);
        }
        return message("上传成功", true, $result);
    }
}